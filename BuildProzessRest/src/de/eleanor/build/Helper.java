package de.eleanor.build;

public class Helper {

	public static final String DATABASE = "eleanor";
	public static final String IP = "132.252.58.192";
	public static final String LOCALHOST = "localhost";
	public static final String PORT = "27017";
	
	public static final String CURRENT_PROJECT_DIR = ".";
    public static final String PROJEKT_DIR = "G:/Projet/Android/EleAnorTmp";
	public static final String FOLDER_PATH = "G:/Projet/Android/EleAnor";
	public static final String TMP_FOLDER_PATH = "G:/Projet/Android/EleAnorTmp";

	public static final String FILE_PATH = "G:/Projet/Android/EleAnorTmp/app/src/nse/res/values/strings.xml";

	//Gradle
	public static final String TASK_ASSEMBLE = "assemble";
	public static final String TASK_BUILD_DEPENDENTS = "buildDependents";
	
	public static final String APK_PATH = "G:/Projet/Android/EleAnorTmp/app/build/outputs/apk";

	//RESTful
	public static final String TARGET = "target???/games/packages";
	
	
	
}
