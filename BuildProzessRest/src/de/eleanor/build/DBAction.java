package de.eleanor.build;

import java.net.UnknownHostException;
import com.mongodb.BasicDBList;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;


public class DBAction {
//	public static final String USERNAME = "eleanor";
//	public static final String PWD = "elean0r";
	
	

	public static DBObject getObject() throws UnknownHostException {
		MongoClient mongoClient = new MongoClient(Helper.LOCALHOST);
		DB db = mongoClient.getDB("eleanor");
		System.out.println("Verbindung zum DB erfolgreich!!!");
		System.out.println("\n" + "Collection lesen...");
		DBCollection dbCollection = db.getCollection("config");
		DBObject config = dbCollection.findOne();
		System.out.println("Collection erfolgreich gelesen");
		System.out.println(config);
		BasicDBList dblist = (BasicDBList) config.get("strings");
		System.out.println(dblist.get(0).toString());
		
		DBObject job=  (DBObject) dblist.get(0);
		return job;
	}
}
