package de.eleanor.build;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


/**
 * 
 * @author duclos
 * 
 * Diese Klasse stellt eine Restful-Schnittstelle f�r das Ausl�sen des Buildprozess zur Verf�gung
 * 
 * @Url: http://localhost:8080/BuildProzessRest/eleanor/buildaction/build
 * 
 */
@Path("/buildaction")
public class BuildAction {

	@GET
	@Path("/build")	
	@Produces({MediaType.APPLICATION_JSON})
	public String build() {

		XMLAction.copy();
		XMLAction.tranformXML();
		GradleAction gradleAction = new GradleAction();
		gradleAction.buildProject();
		//TODO pr�fen, ob der Build erfolgreich war. Wenn Ja den APK-Pfad zum Download zur�ckgeben sonst 
		//eine entsprechende Fehlermeldung
		
		return Helper.APK_PATH;
	}
}
