package de.eleanor.build;
import java.io.File;

import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ProjectConnection;

public class GradleAction {


	public void buildProject() {
		ProjectConnection connection = (ProjectConnection) GradleConnector.newConnector()
										.forProjectDirectory(new File(Helper.PROJEKT_DIR))
										.connect();
		try {
			System.out.println("Build des Projekts " + Helper.PROJEKT_DIR + " gestartet...");
			
			connection.newBuild().forTasks(Helper.TASK_BUILD_DEPENDENTS).run();
			System.out.println("End buildDependents");
			connection.newBuild().forTasks(Helper.TASK_ASSEMBLE).run();			
			
		} finally {
			connection.close();
			System.out.println("Build erfolgreich durchgeführt!");
			System.out.println("Der APK befindet sich unter: " + Helper.APK_PATH);
			
		}
	}
    
}
