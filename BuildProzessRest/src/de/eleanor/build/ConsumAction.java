package de.eleanor.build;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 * 
 * @author duclos
 *
 * Diese Klasse ist f�r Abholung der Minispiel-Packete zust�ndig
 */
public class ConsumAction {
	
	public static boolean consum(){
		
		Client client = ClientBuilder.newClient();
		
		//Helper.TARGET: diese URL muss noch angepasst werden
		WebTarget webTarget = client.target(Helper.TARGET);
		
		ZipInputStream response = webTarget.request(MediaType.APPLICATION_OCTET_STREAM).get(ZipInputStream.class);
		return unzip(response, Helper.FILE_PATH);
	}

	/*
	 * @Philipp diese unzip-Methode kannst du durch deine Methode ersetzen 
	 */
	private static boolean unzip(ZipInputStream zis, String outPutFolder) {
		ZipEntry ze;
		byte[] buffer = new byte[1024];
		
		try {
			ze = zis.getNextEntry();
			while(ze != null){
				String fileName = ze.getName();
				if(! fileName.endsWith("zip") ){
					File newFile = new File(outPutFolder + File.separator + fileName);
					System.out.println("file unzip : " + newFile.getAbsoluteFile());
					
					//nicht existierende Ordner erzeugen
					//Sost wird FileNotFoundException geworfen
					new File(newFile.getParent()).mkdirs();
					FileOutputStream fos = new FileOutputStream(newFile);
					
					int len;
					while((len = zis.read(buffer)) > 0){
							fos.write(buffer, 0, len);
					}
					
					fos.close();
				}
				ze = zis.getNextEntry();
			}
			zis.closeEntry();
			zis.close();
			System.out.println("Fertig!!!");
			
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

}
