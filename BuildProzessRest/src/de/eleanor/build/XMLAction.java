package de.eleanor.build;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.mongodb.DBObject;

public class XMLAction {


	public static void copy() {

		File srcFolder = new File(Helper.FOLDER_PATH);
		File destFolder = new File(Helper.TMP_FOLDER_PATH);

		// make sure source exists
		if (!srcFolder.exists()) {

			System.out.println("Directory does not exist.");
			// just exit
			System.exit(0);

		} else {

			try {
				copyFolder(srcFolder, destFolder);
			} catch (IOException e) {
				e.printStackTrace();
				// error, just exit
				System.exit(0);
			}
		}

		System.out.println("Kopie des Skeleton Fertig!");
	}

	public static void copyFolder(File src, File dest) throws IOException {

		if (src.isDirectory()) {

			// if directory not exists, create it
			if (!dest.exists()) {
				dest.mkdir();
				System.out.println("Directory copied from " + src + "  to " + dest);
			}

			// list all the directory contents
			String files[] = src.list();

			for (String file : files) {
				// construct the src and dest file structure
				File srcFile = new File(src, file);
				File destFile = new File(dest, file);
				// recursive copy
				copyFolder(srcFile, destFile);
			}

		} else {
			// if file, then copy it
			// Use bytes stream to support all file types
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dest);

			byte[] buffer = new byte[1024];

			int length;
			// copy the file content in bytes
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}

			in.close();
			out.close();
			System.out.println("Kopie von " + src + " nach " + dest);
		}
	}

	public static void tranformXML() {
		try {
			
			DBObject job = DBAction.getObject();
			String name = (String) job.get("name");
			String value = (String)job.get("value");
			
			System.out.println("Attribut zu �ndern: " + name);
			System.out.println("Wert zu �ndern: " + value);
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(Helper.FILE_PATH);
			
			NodeList nodeList = doc.getElementsByTagName("string");
			for(int i = 0; i < nodeList.getLength(); i++ ){
				if(nodeList.item(i) != null )
				{
					
					NamedNodeMap attr = nodeList.item(i).getAttributes();
					Node  nodeAttr = attr.getNamedItem("name");
					System.out.println("\n String-"+i + "  "+ nodeAttr.getTextContent());
					
					if(nodeAttr.getTextContent().equals(name)){
						System.out.println(">>Gefunden: " + nodeAttr.getTextContent().equals(name));
						System.out.println(">>Alter Wert: " + nodeList.item(i).getTextContent());
						nodeList.item(i).setTextContent(value);
						System.out.println(">>Neuer Wert: " + nodeList.item(i).getTextContent() + "\n");
						
					}
				}
			}
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(Helper.FILE_PATH));
			transformer.transform(source, result);
			System.out.println("Ersetzung des neuen Wetes im Skeleton fertig!!!");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} 
		catch (TransformerException tfe) {
			tfe.printStackTrace();
		} 
		catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}


	public static void main(String[] args) {
	  copy();
	  tranformXML();
	}
}
